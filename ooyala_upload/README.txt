Ooyala Uploader
===============

This module integrates the Ooyala Web Uploader with the Ooyala Drupal module.
With this module, videos can be uploaded directly to the Ooyala backlot.

Once this module is enabled, an additional "Ooyala uploader" widget will be
available for all Ooyala fields. Select that widget when creating new fields or
when updating existing fields.
